<?php
/*
 * @author: Theun Voets, munsking, rpatit01
 */
// default debugging options
error_reporting(E_ALL);
ini_set("display_errors", 1);

//require_once $_SERVER["DOCUMENT_ROOT"].'/.inc/classes.php';


//clean POST info
$p=filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
$g=filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

session_start();