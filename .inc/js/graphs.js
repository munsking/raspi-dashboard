
var ymin;
var ymax;
var xmin;
var xmax;

$.ajax({
  url:"/.inc/api/data.php",
  method:"POST",
  data:{
    temp:true,
    options:true
  }
}).done(function(data){
  var opts = JSON.parse(data);
  ymin = opts["ymin"];
  ymax = opts["ymax"];
  xmin = opts["xmin"];
  xmax = opts["xmax"];
//  console.log(ymin,ymax,xmin,xmax);
});

function getData(){
  $.ajax({
  url:"/.inc/api/data.php",
  method:"POST",
  data:{
    temp:true,
    data:true
  }
  }).done(function(data){
//    return JSON.parse(data);
    $.plot($("#tempGraph"),[ JSON.parse(data) ],
      {
        yaxis:{
          min:ymin,
          max:ymax
        },
        xaxis:{
          mode:"time",
          timeformat: "%Y-%m-%d %H:%M:%S",
          timezone:"browser"
        },
        series:{
          lines:{
            show:true
          },
          points:{
//            show:true
          }
        },
        grid:{
          hoverable:true,
          clickable:true
        }
      }
    );
  });
}

var debug;

$("#tempGraph").bind("plotclick",function(event,pos,item){
  $("#tempTooltip").html("temp: "+pos.y.toFixed(2)+"&#8451;<br />at: i suck at dates lol");
});
$("#tempGraph").bind("plothover",function(event,pos,item){
  $("#tempTooltip").html("temp: "+pos.y.toFixed(2)+"&#8451;<br />at: i suck at dates lol");
});


var updateTemp = setInterval(function(){
  getData();
//  console.log("updated");
},1000);